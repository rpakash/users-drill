const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interest: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

function videoGame(users) {
  return Object.entries(users)
    .map(([person, details]) => {
      details.name = person;
      return details;
    })
    .filter((person) => {
      if (person["interests"] === undefined) {
        return person["interest"].toString().includes("Video");
      } else {
        return person["interests"].toString().includes("Video");
      }
    });
}

function userPlaces(users, place) {
  return Object.entries(users).reduce((accu, user) => {
    if (user[1].nationality === place) {
      accu[user[0]] = user[1];
    }

    return accu;
  }, {});
}

function sortUsers(users, sortByDesignation) {
  if (sortByDesignation) {
    return Object.entries(users)
      .map(([user, details]) => {
        details["name"] = user;
        return details;
      })
      .sort((person1, person2) => {
        return person2.desgination.localeCompare(person1.desgination);
      });
  } else {
    return Object.entries(users)
      .map(([user, details]) => {
        details["name"] = user;
        return details;
      })
      .sort((person1, person2) => {
        return person2.age - person1.age;
      });
  }
}

function usersDegree(users) {
  return Object.entries(users).filter(([user, details]) => {
    return details.qualification.includes("Masters");
  });
}
